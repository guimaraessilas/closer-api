/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.closer.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author silas
 */


public class PgConnection {

    private static final String USERNAME = "postgres";
    private static final String DATABASE = "closer";
    private static final String PASSWORD = "postgres";
    private static final String DATABASE_URL = "jdbc:postgresql://localhost:5432/";
    
    private static Connection c = null;

    public static Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            if (c == null) {
                c = DriverManager.getConnection(DATABASE_URL + DATABASE, USERNAME, PASSWORD);
                System.out.println("Conexão Nova");
            } else {
                if (c.isClosed()) {
                    c = DriverManager.getConnection(DATABASE_URL + DATABASE, USERNAME, PASSWORD);
                }
                System.out.println("Conexão Existente!");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
        }

        return c;
    }

    public static void main(String[] args){
        System.out.println(getConnection() != null);
    }
}
