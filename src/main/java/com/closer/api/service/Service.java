package com.closer.api.service;

import com.closer.api.dao.ContactDAO;
import com.closer.api.dao.UserProfileDAO;
import com.closer.api.model.Contact;
import com.closer.api.model.UserProfile;
import com.closer.utils.Json;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author r2d2
 */
public class Service {

    public Json signup(UserProfile userProfile) {
        UserProfileDAO dao = new UserProfileDAO();
        Json json = new Json();
        if(dao.list("where email = '"+userProfile.getEmail()+"';").isEmpty()){
            if(dao.list("where username = '"+userProfile.getUsername()+"';").isEmpty()){
                userProfile = dao.insert(userProfile);
            }else{
                json.setMessage("Escolha outro username");
            }
        }else{
            json.setMessage("Este email já está sendo usado por outro usuário.");
        }
        json.setJson(userProfile);
        json.setResult(userProfile.getId() != null);
        return json;
    }
    
    public Json login(UserProfile userProfile) {
        UserProfileDAO dao = new UserProfileDAO();
        Json json = new Json();
        List<UserProfile> listUserProfile = dao.list("where (email = '"+userProfile.getEmail()+"' or username = '"+userProfile.getEmail()+"') and password = '"+userProfile.getPassword()+"';");
        json.setJson(listUserProfile);
        if(listUserProfile.isEmpty()){
            json.setMessage("Verifique suas credenciais e tente novamente!");
        }else{
            json.setMessage("Login realizado!");
        }
        json.setResult(!listUserProfile.isEmpty());
        return json;
    }

    public Json updateProfile(UserProfile userProfile) {
        UserProfileDAO dao = new UserProfileDAO();
        Json json = new Json();
        
        userProfile = dao.update(userProfile);
        json.setJson(userProfile);
        json.setResult(userProfile.getId() != null);
        if(json.isResult()){
            json.setMessage(" Usuario Atualizado ");
        }else{
            json.setMessage(" Erro ao atualizar o usuario. ");
        }
        
        return json;
    }

    public Json searchUsers(String search) {
        UserProfileDAO dao = new UserProfileDAO();
        Json json = new Json();
        List<UserProfile> listUserProfile = dao.list("where email like '%"+search+"%' or username like '%"+search+"%' or fullname like '%"+search+"%' or phone like '%"+search+"%';");
        json.setJson(listUserProfile);
        if(listUserProfile.isEmpty()){
            json.setMessage(" Nenhum usuario correspondente... ");
        }else{
            json.setMessage("Aqui estao os usuarios encontrados!");
            
        }
        json.setResult(!listUserProfile.isEmpty());
        return json;
    }
    
    public Json listContacts(String search) {
        ContactDAO dao = new ContactDAO();
        Json json = new Json();
        List<Contact> listContact = new ArrayList<>();
        if(StringUtils.isNumericSpace(search)){
            listContact = dao.list("where user_id = "+search+";");
        }else{
            listContact = dao.list("where username like '%"+search+"%';");
        }
        
        json.setJson(listContact);
        if(listContact.isEmpty()){
            json.setMessage(" Nenhum contato correspondente... ");
        }else{
            json.setMessage("Aqui estao os usuarios encontrados!");
            
        }
        json.setResult(!listContact.isEmpty());
        return json;
    }

    public Json addContact(Contact contact) {
        ContactDAO dao = new ContactDAO();
        Json json = new Json();
        List<Contact> listContact = new ArrayList<>();
        listContact = dao.list("where (user_id = '"+contact.getUserId()+"' and contact_id = '"+contact.getContactId()+"')");
        
        if(listContact.isEmpty()){
            contact.setStatus("Pendente");
            contact = dao.insert(contact);
        }else{
            contact.setId(listContact.get(0).getId());
            contact = dao.update(contact);
        }
        
        json.setJson(contact);
        json.setResult(contact.getId() != null);
        json.setMessage("Nao foi possivel completar a açao.\nVerifique sua conexao e tente novamente mais tarde!");
        if(json.isResult()){
            json.setMessage("Solicitaçao de amizade enviada!");
        }
        return json;

    }
    
}