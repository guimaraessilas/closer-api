/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.closer.api.service;

import com.closer.api.model.Contact;
import com.closer.api.model.UserProfile;
import com.closer.utils.Json;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author silas
 */
@Path("service")
public class WebService {

    @Context
    private UriInfo context;

    public WebService() {}
    
    //Auth
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/signup")
    public Response signUp(String content) {
        Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy'T'HH:mm:ss").create();
        Json json = new Json();
        UserProfile userProfile = gson.fromJson(content, UserProfile.class);
        Service service = new Service();
        json = service.signup(userProfile);
        
        return Response.status(200).entity(gson.toJson(json)).header("Access-Control-Allow-Origin", "*").build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/login")
    public Response login(String content) {
        Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy'T'HH:mm:ss").create();
        Json json = new Json();
        UserProfile userProfile = gson.fromJson(content, UserProfile.class);
        Service service = new Service();
        json = service.login(userProfile);
        
        return Response.status(200).entity(gson.toJson(json)).header("Access-Control-Allow-Origin", "*").build();
    }
    
    //USER
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/user")
    public Response updateProfile(String content) {
        Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy'T'HH:mm:ss").create();
        Json json = new Json();
        UserProfile userProfile = gson.fromJson(content, UserProfile.class);
        Service service = new Service();
        json = service.updateProfile(userProfile);
        
        return Response.status(200).entity(gson.toJson(json)).header("Access-Control-Allow-Origin", "*").build();
    }
    
    //CONTACTS
    @GET
    @Produces("application/json")
    @Path("/user/{search}")
    public Response searchUsers(@PathParam("search") String search) {
        Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy'T'HH:mm:ss").create();
        Service service = new Service();
        Json json = new Json();
        json = service.searchUsers(search);
        
        return Response.status(200).entity(gson.toJson(json)).header("Access-Control-Allow-Origin", "*").build();
    }
    
    @GET
    @Produces("application/json")
    @Path("/contact/{search}")
    public Response listContacts(@PathParam("search") String search) {
        Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy'T'HH:mm:ss").create();
        Service service = new Service();
        Json json = new Json();
        json = service.listContacts(search);
        
        return Response.status(200).entity(gson.toJson(json)).header("Access-Control-Allow-Origin", "*").build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/contact")
    public Response addContact(String content) {
        Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy'T'HH:mm:ss").create();
        Json json = new Json();
        Contact contact = gson.fromJson(content, Contact.class);
        Service service = new Service();
        json = service.addContact(contact);
        
        return Response.status(200).entity(gson.toJson(json)).header("Access-Control-Allow-Origin", "*").build();
    }
}