/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.closer.api.dao;

import com.closer.api.model.UserProfile;
import com.closer.utils.PgConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author silas
 */
public class UserProfileDAO {

    public UserProfile insert(UserProfile userProfile) {
        try {
            PgConnection conexao = new PgConnection();
            Connection conn = conexao.getConnection();

            String sql = "insert into user_profile"
                    + " (username, current_location, password, email, fullname, bio, profile_photo_path)"
                    + " values "
                    + " (?,?,?,?,?,?,?)";

            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            System.out.println(sql);
            
            ps.setString(1, userProfile.getUsername());
            ps.setString(2, userProfile.getCurrentLocation());
            ps.setString(3, userProfile.getPassword());
            ps.setString(4, userProfile.getEmail());
            ps.setString(5, userProfile.getFullname());
            ps.setString(6, userProfile.getBio());
            ps.setString(7, userProfile.getProfilePhotoPath());

            ps.execute();

            ResultSet rsID = ps.getGeneratedKeys();
            if (rsID.next()) {
                Long seq = rsID.getLong("id");
                System.out.println("Id: "+seq);
                userProfile.setId(seq);
            }

            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(UserProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }
        return userProfile;
    }

    public UserProfile update(UserProfile userProfile) {
        try {
            PgConnection conexao = new PgConnection();
            Connection conn = conexao.getConnection();
            String sql = "update user_profile set username = ?, current_location = ?, email = ?, fullname = ?, bio = ? where id = ?";
            System.out.println(sql);
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, userProfile.getUsername());
            ps.setString(2, userProfile.getCurrentLocation());
            ps.setString(3, userProfile.getEmail());
            ps.setString(4, userProfile.getFullname());
            ps.setString(5, userProfile.getBio());
            ps.setLong(6, userProfile.getId());
            
            ps.execute();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(UserProfile.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
            return new UserProfile();
        }

        return userProfile;
    }

    public List<UserProfile> list(String condicao) {
        try {
            PgConnection conexao = new PgConnection();
            Connection conn = conexao.getConnection();
            String sql = "SELECT * FROM user_profile "+ condicao;

            System.out.println(sql);

            List<UserProfile> userList = new ArrayList<>();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                UserProfile userProfile = new UserProfile();
                
                userProfile.setId(rs.getLong("id"));
                userProfile.setUsername(rs.getString("username"));
                userProfile.setFullname(rs.getString("fullname"));
                userProfile.setEmail(rs.getString("email"));
                userProfile.setBio(rs.getString("bio"));
                userProfile.setProfilePhotoPath(rs.getString("profile_photo_path"));
                userProfile.setPhone(rs.getString("phone"));
                
                userList.add(userProfile);
            }

            ps.execute();
            ps.close();

            return userList;
        } catch (SQLException ex) {
            Logger.getLogger(UserProfile.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());                     // Print da mensagem de tratamento de erro.
            return null;
        }
    }

    public boolean deletar(UserProfile userProfile) {
        try {
            PgConnection conexao = new PgConnection();
            Connection conn = conexao.getConnection();
            String sql = "DELETE FROM user_profile WHERE id =  ? ";

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setLong(1, userProfile.getId());

            ps.execute();
            ps.close();

            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }
}