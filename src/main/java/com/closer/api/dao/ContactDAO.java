/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.closer.api.dao;

import com.closer.api.model.Contact;
import com.closer.api.model.UserProfile;
import com.closer.utils.PgConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author silas
 */
public class ContactDAO {

    public Contact insert(Contact contact) {
        try {
            PgConnection conexao = new PgConnection();
            Connection conn = conexao.getConnection();

            String sql = "insert into contact"
                    + " (user_id, contact_id, status, username, profile_photo_path, current_location)"
                    + " values "
                    + " (?,?,?,?,?,?,?)";

            PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ps.setLong(1, contact.getUserId());
            ps.setLong(2, contact.getContactId());
            ps.setString(3, contact.getStatus());
            ps.setString(4, contact.getUserName());
            ps.setString(5, contact.getProfilePhotoPath());
            ps.setString(6, contact.getCurrentLocation());
            
            ps.execute();

            ResultSet rsID = ps.getGeneratedKeys();
            if (rsID.next()) {
                Long seq = rsID.getLong("id");
                contact.setId(seq);
            }

            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(UserProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }
        return contact;
    }

    public Contact update(Contact contact) {
        try {
            PgConnection conexao = new PgConnection();
            Connection conn = conexao.getConnection();
            String sql = "update contact set status = ?, username = ?, profile_photo_path = ? where user_id = ? and contact_id = ?";

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, contact.getStatus());
            ps.setString(2, contact.getUserName());
            ps.setString(3, contact.getProfilePhotoPath());
            ps.setLong(4, contact.getUserId());
            ps.setLong(5, contact.getContactId());
            
            ps.execute();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(UserProfile.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }

        return contact;
    }

    public List<Contact> list(String condicao) {
        try {
            PgConnection conexao = new PgConnection();
            Connection conn = conexao.getConnection();
            String sql = "SELECT * FROM contact "+ condicao;

            System.out.println(sql);

            List<Contact> contactList = new ArrayList<>();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Contact contact = new Contact();
                
                contact.setId(rs.getLong("id"));
                contact.setUserName(rs.getString("username"));
                contact.setProfilePhotoPath(rs.getString("profile_photo_path"));
                contact.setUserId(rs.getLong("user_id"));
                contact.setContactId(rs.getLong("contact_id"));
                contact.setStatus(rs.getString("status"));
                contact.setCurrentLocation(rs.getString("current_location"));
                
                
                contactList.add(contact);
            }

            ps.execute();
            ps.close();

            return contactList;
        } catch (SQLException ex) {
            Logger.getLogger(UserProfile.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());                     // Print da mensagem de tratamento de erro.
            return null;
        }
    }

    public boolean deletar(Contact contact) {
        try {
            PgConnection conexao = new PgConnection();
            Connection conn = conexao.getConnection();
            String sql = "DELETE FROM contact WHERE id =  ? ";

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setLong(1, contact.getId());

            ps.execute();
            ps.close();

            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }
}